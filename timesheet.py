import tabula
import json
import re
import sys
import signal
import os


def main():

  signal.signal(signal.SIGINT, signal_handler)

  while True:

    fileName = input("Enter the name of the Rocla timesheet pdf file (Ctrl+c to exit): ")    

    try:
      print(f"Converting {fileName} to a json file")
      tabula.convert_into(fileName, fileName+".json", output_format="json", pages="all", silent=True)
    except Exception as ex:
      print(f"Failed to convert the pdf file to a json file\n{ex}\n")
      continue

    try:
      with open(fileName+".json", "r") as f:
        fileAsJson = json.load(f)
    except Exception as ex:
      print(f"Failed to open the converted json file\n{ex}")
      continue


    #dataList = fileAsJson[0]["data"]
    resultDict = getDaysAndDayLengths(fileAsJson)
    cleanUp(fileName)
    writeToFile(fileName, resultDict)

def getDaysAndDayLengths(pageList):
  resultDict = {}
  time_re = re.compile(r'^([0-1][0-9]|[2][0-3]):([0-5][0-9])$')
  for page in pageList:
    dataList = page["data"]
    for listItem in dataList:
      if listItem[0]["text"].startswith(("Ma", "Ti", "Ke", "To", "Pe")):
        for item in listItem:
          if bool(time_re.match(item["text"])):
            timeInString = item["text"]
            timeList = timeInString.split(":")
            timeInHours = round(int(timeList[0]) + int(timeList[1]) / 60, 2)
            resultDict[listItem[0]["text"]] = timeInHours
  return resultDict
    
def signal_handler(signal, frame):
  sys.exit(0)

def cleanUp(fileName):
  print("Deleting the conversion file")
  os.remove(fileName+".json")

def writeToFile(fileName, resultDict):
  print(f"Writing the dates and work day lengths in hours to a json file {fileName}.json")
  try:
    with open(fileName+".json", "w") as fp:
      json.dump(resultDict, fp, indent=4)
  except Exception as ex:
    print(f"Failed to write the results to a file\n{ex}")


  
if __name__ == "__main__":
    main()