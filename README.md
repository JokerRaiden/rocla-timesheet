<h1>Rocla timesheet reader</h1>
<h2>Installation instructions</h2>
1. Make sure you have jdk installed<br/>
2. Make sure you have added java installation folder to path system variable. E.g. C:\Program Files\Java\jdk-13.0.2\bin<br/>
3. Make sure you have python 3.5+ installed<br/>
4. pip install tabula-py